"use client";
import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import NavBar from "./components/Nav/navbar";
import { store } from "../store/store";
import { Provider } from "react-redux";
import "./styles/index.css";
import { AuthContextProvider } from "@/context/AuthContext";

const inter = Inter({ subsets: ["latin"] });

const metadata: Metadata = {
  title: "ShopZenith",
  description: "ShopZenith",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={inter.className} suppressHydrationWarning={true}>
        <AuthContextProvider>
          <Provider store={store}>
            <NavBar />
            {children}
          </Provider>
        </AuthContextProvider>
      </body>
    </html>
  );
}
