/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @next/next/no-img-element */
"use client";
import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import type { RootState } from "../../../store/store";
import { useParams } from "next/navigation";
import { Anybody } from "next/font/google";
import { ProductState } from "@/store/productSlice";
import { getData } from "@/app/routes";
import { addProducts, addToCart } from "@/store/productSlice";
import Loading from "@/app/components/loading";
import { useRouter } from "next/navigation";

function ProductDescription() {
  const params = useParams<{ id: string }>();
  const { id } = params;
  const products = useSelector((state: RootState) => state.products);
  const [item, setItem] = useState<ProductState>(init());
  const router = useRouter();

  const dispatch = useDispatch();

  function init() {
    const init = {
      id: 0,
      title: "",
      price: 0,
      description: "",
      image: "",
      category: "",
      rating: null,
      isCartAdded: false,
      qty: 0,
    };
    return init;
  }
  useEffect(() => {
    let productArr = products.products;
    if (productArr?.length) {
      let product = productArr.find((item: { id: number; }) => item.id === +id);
      setItem(product ? product : init());
    }
  }, [products, id]);

  useEffect(() => {
    async function getProduct() {
      let res = await getData();
      return res;
    }
    if (products.products.length === 0) {
      getProduct().then((res) => {
        dispatch(addProducts(res));
      });
    }
  }, []);

  const handleCartItems = () => {
    dispatch(addToCart(item));
  };

  const handleNavigateToCart = () => {
    router.push("/cart");
  };

  return (
    <div className="min-h-screen">
      {item?.id ? (
        <div className="flex justify-center">
          <div className="ms-16 mt-16">
            <img
              src={item.image}
              alt="item"
              className="object-fill mix-blend-multiply h-96 w-96 rounded-xl"
            />
          </div>
          <div className="w-[50%] mt-10 ms-5 flex flex-col justify-center">
            <h1 className="font-bold mb-2">{item.title}</h1>
            <p className="flex">
              <span className="me-3">{item.rating.rate} &#9733; </span>
              <span>{item.rating.count} ratings</span>
            </p>
            <h1 className="my-2 font-bold text-4xl">&#8377;{item.price}</h1>
            <p className="my-2">{item.description}</p>
            <p>{item.category}</p>
            <div>
              <button
                className={`${
                  item.isCartAdded
                    ? `bg-orange-400 hover:bg-orange-500 border-orange-400 hover:border-orange-500`
                    : `bg-blue-500 hover:bg-blue-700 border-blue-500 hover:border-blue-700`
                } text-white font-bold py-2 px-4 border rounded mt-3`}
                onClick={
                  item.isCartAdded ? handleNavigateToCart : handleCartItems
                }
              >
                {item.isCartAdded ? "Go to Cart" : "Add to Cart"}
              </button>
            </div>
          </div>
        </div>
      ) : (
        <div>
          <Loading />
        </div>
      )}
    </div>
  );
}

export default ProductDescription;
