"use client";
import React, { useState } from "react";
import { FormProvider, useForm } from "react-hook-form";
import Image from "next/image";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { Eye } from "lucide-react";
import { EyeOff } from "lucide-react";
import signUp from "@/firebase/auth/signup";
import signIn from "@/firebase/auth/signin";
import { useRouter } from "next/navigation";
import { ToastContainer, toast } from "react-toastify";
import Loading from "../components/loading";

import "react-toastify/dist/ReactToastify.css";

const schema = yup.object().shape({
  email: yup
    .string()
    .email("Please enter a valid email address")
    .required("Email is required"),
  password: yup
    .string()
    .min(8, "password must contain 8 characters")
    .matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
      "Password requires: 1 uppercase, 1 lowercase, 1 number, 1 special character"
    )
    .required("password is required"),
});

type Inputs = {
  email: string;
  password: string;
};

function Login() {
  const router = useRouter();

  const [singUp, setSignUp] = useState(false);
  const [show, setShow] = useState(false);
  const [Loader, setLoader] = useState(false);

  const form = useForm<Inputs>({
    resolver: yupResolver(schema),
  });
  const {
    register,
    formState: { errors },
  } = form;

  const onSubmit = async (data: any) => {
    setLoader(true);
    const { email, password } = data;
    let res, err;
    if (singUp) {
      const { result, error } = await signUp(email, password);
      res = result;
      err = error;
    } else {
      const { result, error } = await signIn(email, password);
      res = result;
      err = error;
    }
    if (err) {
      setLoader(false);
      console.error("err", err);
      toast("Invalid User", { type: "error" });
      return;
    }
    toast("Login Successful", { type: "success" });
    router.push("/");
  };

  return (
    <div className=" h-[90vh] flex justify-center items-center">
      {Loader && (
        <div className="absolute top-0 left-0 w-full h-full flex items-center justify-center bg-black bg-opacity-25 z-20">
          <Loading />
        </div>
      )}
      <ToastContainer />
      <FormProvider {...form}>
        <div className="relative flex flex-col items-center w-[550px] justify-center overflow-hidden shadow-lg">
          <div className="w-full p-6 bg-white rounded-md shadow-md lg:max-w-xl">
            <Image
              className="m-auto rounded"
              src="/ZenithLogo.jpg"
              alt="ZenithLogo"
              width={120}
              height={10}
            />
            <form className="mt-6" onSubmit={form.handleSubmit(onSubmit)}>
              <div className="mb-2">
                <label
                  htmlFor="email"
                  className="block text-sm font-semibold text-gray-800"
                >
                  Email
                </label>
                <input
                  {...register("email")}
                  name="email"
                  type="email"
                  className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border rounded-md focus:border-gray-400 focus:ring-gray-300 focus:outline-none focus:ring focus:ring-opacity-40"
                />
                {errors.email?.message ? (
                  <span className="text-red-600 text-sm">
                    {errors.email?.message}
                  </span>
                ) : (
                  <span>&nbsp;</span>
                )}
              </div>
              <div className="mb-2 relative">
                <label
                  htmlFor="password"
                  className="block text-sm font-semibold text-gray-800"
                >
                  Password
                </label>
                <input
                  {...register("password")}
                  name="password"
                  type={show ? "text" : "password"}
                  className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border rounded-md focus:border-gray-400 focus:ring-gray-300 focus:outline-none focus:ring focus:ring-opacity-40"
                />
                <button
                  type="button"
                  onClick={() => setShow(!show)}
                  className="absolute inset-y-0 right-0 px-3 py-2 text-gray-600"
                >
                  {show ? <EyeOff /> : <Eye />}
                </button>
                {errors.password?.message ? (
                  <span className="text-red-600 text-sm">
                    {errors.password?.message}
                  </span>
                ) : (
                  <span>&nbsp;</span>
                )}
              </div>
              <div className="mt-2">
                <button className="w-full px-4 py-2 tracking-wide text-white transition-colors duration-200 transform bg-gray-700 rounded-md hover:bg-gray-600 focus:outline-none focus:bg-gray-600">
                  {singUp ? "Create Account" : "Login"}
                </button>
              </div>
            </form>
            <p className="mt-4 text-sm text-center text-gray-700">
              {singUp ? `Already` : `Don't`} have an account?{" "}
              <span
                onClick={() => setSignUp(!singUp)}
                className="text-blue-500 cursor-pointer hover:underline"
              >
                {singUp ? "Sign In" : "Sign up"}
              </span>
            </p>
          </div>
        </div>
      </FormProvider>
    </div>
  );
}

export default Login;
