import React from "react";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";

interface DiologueProps {
  title?: string;
  open: boolean;
  onClose: () => void;
  content: any;
  size?: string;
}

function CustomModal({ title, open, onClose, content, size }: DiologueProps) {
  return (
    <Dialog open={open} onOpenChange={onClose}>
      <DialogContent className={`${size}`}>
        <DialogHeader>
          <DialogTitle>{title}</DialogTitle>
          <DialogDescription>{content}</DialogDescription>
        </DialogHeader>
      </DialogContent>
    </Dialog>
  );
}

export default CustomModal;
