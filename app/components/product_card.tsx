/* eslint-disable @next/next/no-img-element */
import Link from "next/link";
import React from "react";

function ProductCard(props: any) {
  const { product } = props;
  return (
    <div className="w-1/5 m-2 border-blue-300 border-2 p-3 px-5 rounded-xl text-center">
      <Link href={`/products/${product?.id}`}>
        <img
          src={product?.image}
          alt="product"
          className="object-fill mix-blend-multiply h-[300px] w-[300px] rounded-xl m-auto card-image"
        />
      </Link>
      <Link href={`/products/${product?.id}`}>
        <h3 className="text-center mt-3 hover:text-blue-500">
          {product.title}
        </h3>
      </Link>
    </div>
  );
}

export default ProductCard;
