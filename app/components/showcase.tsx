/* eslint-disable @next/next/no-img-element */
import React, { useEffect } from "react";
import { ArrowRight } from "lucide-react";
import { gsap } from "gsap";

function ShowCase() {
  useEffect(() => {
    gsap.to(".go-button", {
      duration: 1,
      repeat: -1,
      scale: 1.5,
      yoyo: true
    });
  }, []);

  return (
    <div className="bg-[#EEF5FF] p-16 text-center">
      <div className="text-3xl">
        <div>
          <h1 className="px-10 text-black mb-1">
            Elevate your shopping journey to new heights with ShopZenith.
            Immerse yourself in a curated selection of top-quality products,
            handpicked to meet your every need and desire. Shop with confidence
            and convenience at ShopZenith
          </h1>
        </div>
      </div>
      <button
        aria-label="go to products"
        className="bg-black text-white rounded-full p-3 mt-3 go-button"
      >
        <ArrowRight size={25} />
      </button>
    </div>
  );
}

export default ShowCase;
