import { getData } from "@/app/routes";
import React, { useEffect, useState } from "react";
import ProductCard from "./product_card";
import { addProducts } from "@/store/productSlice";
import { useSelector, useDispatch } from "react-redux";
import type { RootState } from "../../store/store";
import { ProductState } from "@/store/productSlice";

function Product() {
  const [products, setProducts] = useState<ProductState[]>([]);
  const dispatch = useDispatch();
  const storeItems = useSelector((state: RootState) => state.products);

  useEffect(() => {
    async function getProduct() {
      let res = await getData();
      return res;
    }
    if (storeItems.products.length === 0) {
      getProduct().then((res) => {
        dispatch(addProducts(res));
      });
    }
  }, []);

  useEffect(() => {
    setProducts(storeItems.products);
  }, [storeItems]);

  return (
    <div className="min-h-screen flex flex-wrap justify-evenly mt-3">
      {products.map((product: any) => (
        <ProductCard key={product?.id} product={product} />
      ))}
    </div>
  );
}

export default Product;
