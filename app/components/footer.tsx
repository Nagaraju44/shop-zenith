import React from "react";
import { FaFacebook } from "react-icons/fa";
import { FaInstagram } from "react-icons/fa6";
import { FaXTwitter } from "react-icons/fa6";
import { IoLogoLinkedin } from "react-icons/io5";

function Footer() {
  return (
    <div className="h-[35vh] bg-gray-700 text-white flex justify-evenly items-center">
      <div>
        <h1 className="text-lg font-bold mb-4">About ShopZenith</h1>
        <ul>
            <li className="mb-2 cursor-pointer hover:text-blue-500">About Us</li>
            <li className="mb-2 cursor-pointer hover:text-blue-500">Promotion Details</li>
            <li className="mb-2 cursor-pointer hover:text-blue-500">Reviews</li>
            <li className="cursor-pointer hover:text-blue-500">Careers</li>
        </ul>
      </div>
      <div>
      <h1 className="text-lg font-bold mb-4">Customer Care</h1>
        <ul>
            <li className="mb-2 cursor-pointer hover:text-blue-500">Tracking</li>
            <li className="mb-2 cursor-pointer hover:text-blue-500">Shipping</li>
            <li className="mb-2 cursor-pointer hover:text-blue-500">Returns</li>
            <li className="cursor-pointer hover:text-blue-500">Contact Us</li>
        </ul>
      </div>
      <div>
        <h1 className="text-lg font-bold mb-4">Follow Us On</h1>
        <div className="flex justify-evenly">
          <FaFacebook size={40} className="me-2 cursor-pointer text-[#4267B2]" />
          <FaInstagram size={40} className="me-2 cursor-pointer text-[#f21616]" />
          <FaXTwitter size={40} className="me-2 cursor-pointer text-[#fff]" />
          <IoLogoLinkedin size={40} className="me-2 cursor-pointer text-[#137ED9]" />
        </div>
      </div>
    </div>
  );
}

export default Footer;
