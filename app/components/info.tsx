import React, { useEffect } from "react";
import { ArrowRight } from "lucide-react";
import { gsap } from "gsap";

function InfoCard() {
  useEffect(() => {
    const tl = gsap.timeline({});
    tl.to(".title", { duration: 3, text: "ShopZenith" })
    .to(".showcase", {duration: 3, text: "We have a best deals for you today", delay: 1, repeat: -1, repeatDelay:3})
    .to(".showcase", {duration: 6, text: "Your One-Stop Shop for Everything", delay: 2, repeat: -1, repeatDelay:6});
  }, []);

  return (
    <div className="h-[80vh] w-[100%] relative">
      <video
        autoPlay
        muted
        loop
        playsInline
        preload="auto"
        className="w-[100%] h-[100%] object-cover"
      >
        <source src="pexels.mp4" type="video/mp4" />
      </video>
      <div className="absolute inset-0 bg-black opacity-75 z-1 flex justify-center items-center">
        <div className="text-center">
          <h1 className="text-white text-[60px] title"></h1>
          <p className="text-white text-[30px] showcase">
            Your One-Stop Shop for Everything
          </p>
          <button className="flex items-center m-auto opacity-100 text-bold relative z-10 border-4 border-white rounded-lg bg-transparent p-3 mt-4 text-white hover:bg-white hover:text-black hover:border-transparent transition-all">
            Show All Products
            <ArrowRight size={20} />
          </button>
        </div>
      </div>
    </div>
  );
}

export default InfoCard;
