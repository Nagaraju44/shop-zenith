import React from "react";
import { Oval } from 'react-loader-spinner';
function Loading() {
  return (
    <div className="min-h-screen flex flex-row justify-center items-center">
      <Oval
        visible={true}
        height="60"
        width="60"
        color="#000000"
        ariaLabel="oval-loading"
        wrapperStyle={{}}
        wrapperClass=""
        secondaryColor="#ffffff"
      />
    </div>
  );
}

export default Loading;
