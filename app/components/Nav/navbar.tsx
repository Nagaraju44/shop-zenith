"use client";
import React, { useEffect } from "react";
import Link from "next/link";
import { redirect, usePathname } from "next/navigation";
import { useSelector } from "react-redux";
import type { RootState } from "../../../store/store";
import { LogOut } from "lucide-react";
import { signOut } from "firebase/auth";
import { auth } from "@/context/AuthContext";
import { toast } from "react-toastify";
import { useAuthContext } from "@/context/AuthContext";

function NavBar() {
  const pathname = usePathname();
  const cartItems = useSelector((state: RootState) => state.products.cart);

  const { user }: any = useAuthContext();

  useEffect(() => {
    const handleScroll = () => {
      const navbar = document.getElementById("navbar");
      if (navbar) {
        if (window.scrollY > 15) {
          // navbar.classList.add("bg-white");
        }
      }
    };
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const handleLogout = () => {
    signOut(auth)
      .then(() => {
        toast("Logout Successful", { type: "success" });
        redirect("/login");
      })
      .catch((error) => {
        console.log("error", error);
      });
  };

  return (
    <>
      {user ? (
        <div
          id="navbar"
          className={`sticky top-0 left-0 right-0 bg-gray-200 z-50`}
        >
          <ul className="flex ">
            <li className="m-5 font-medium">
              <Link
                className={`link ${
                  pathname === "/" || pathname?.startsWith("/product")
                    ? "text-blue-700"
                    : ""
                }`}
                href={"/"}
              >
                Products
              </Link>
            </li>
            <li className="m-5 font-medium">
              <Link
                className={`link ${
                  pathname === "/cart" ? "text-blue-700" : ""
                }`}
                href={"/cart"}
              >
                Cart
                <sup
                  className={
                    cartItems.length > 0
                      ? `bg-red-600 text-sm py-[1.7px] px-[6px] rounded-[50%] text-white`
                      : ``
                  }
                >
                  {cartItems.length > 0 && cartItems.length}
                </sup>
              </Link>
            </li>
            <li className="m-5 font-medium">
              <Link
                className={`link ${
                  pathname === "/sales" ? "text-blue-700" : ""
                }`}
                href={"/sales"}
              >
                Sales
              </Link>
            </li>
            <li className="m-5 font-medium">
              <Link
                className={`link ${
                  pathname === "/payments" ? "text-blue-700" : ""
                }`}
                href={"/payments"}
              >
                Payments
              </Link>
            </li>
          </ul>
          <div onClick={handleLogout}>
            <LogOut className="absolute right-8 top-5 cursor-pointer hover:text-blue-500" />
          </div>
        </div>
      ) : (
        <></>
      )}
    </>
  );
}

export default NavBar;
