/* eslint-disable @next/next/no-img-element */
import {
    ProductState,
    removeFromCart,
    IncreaseQty,
    decreaseQty,
    EnterQty,
  } from "@/store/productSlice";
  import React, { useEffect, useState } from "react";
  import Link from "next/link";
  import { useDispatch } from "react-redux";
  
  function Item(props: { cartItem: ProductState }) {
    const dispatch = useDispatch();
    const [Qty, setQty] = useState<number>(props.cartItem.qty);
    const calculateDeliveryDate = () => {
      const today = new Date();
      const deliveryDate = new Date(
        today.getFullYear(),
        today.getMonth(),
        today.getDate() + 7
      );
      const formattedDate = new Intl.DateTimeFormat("en-US", {
        weekday: "short",
        month: "short",
        day: "2-digit",
      }).format(deliveryDate);
      return formattedDate;
    };
  
    const calculateMorePrice = (price: number) => {
      return ((price * 110) / 100).toFixed(2);
    };
  
    const handleRemoveCartItem = () => {
      dispatch(removeFromCart(props.cartItem?.id));
    };
  
    const handleIncreaseQty = () => {
      dispatch(IncreaseQty(props.cartItem?.id));
    };
  
    const handleDecreaseQty = () => {
      dispatch(decreaseQty(props.cartItem?.id));
    };
  
    const handleQtyChange = (event: any) => {
      if (!isNaN(event.target.value)) {
        setQty(+event.target.value);
      }
    };
  
    const updateQtyChange = () => {
      dispatch(EnterQty({ id: props.cartItem?.id, qty: Qty }));
    };
  
    useEffect(() => {
      setQty(props.cartItem.qty);
    }, [props.cartItem.qty]);
  
    return (
      <div
        key={props.cartItem.id}
        className="bg-white self-end py-3 border-[1px] border-slate-200"
      >
        <div className="flex justify-evenly">
          <img
            src={props.cartItem.image}
            width={75}
            height={75}
            className="object-contain mix-blend-multiply"
            alt="cartImage"
          />
          <div className="self-center w-[50%]">
            <Link href={`/products/${props.cartItem?.id}`}>
              <p className="hover:text-blue-600">{props.cartItem.title}</p>
            </Link>
            <div className="flex items-center">
              <span className="text-gray-500">Seller: Relianece India</span>
              <img
                src="https://img.freepik.com/premium-vector/best-seller-badge-best-seller-golden-label-retail-badge-advertisement-symbol-vector-stock-illustration_100456-10882.jpg?w=740"
                alt="bestseller"
                height={40}
                width={40}
              />
            </div>
            <div className="flex items-center">
              <span className="me-2 line-through text-gray-500">
                &#8377;{calculateMorePrice(props.cartItem.price)}
              </span>
              <h1 className="my-2 me-3 font-bold text-xl">
                &#8377;{props.cartItem.price}
              </h1>
              <p className="text-green-600 font-medium">10% Off</p>
            </div>
          </div>
          <div className="">
            <p>Delivery by {calculateDeliveryDate()}</p>
          </div>
        </div>
        <div className="flex mt-2">
          <div className="me-5 pl-9">
            <button
              onClick={handleDecreaseQty}
              className="rounded-full border-[1px] border-slate-200 p-1 px-3"
              disabled={props.cartItem.qty === 1}
            >
              -
            </button>
            <input
              type="text"
              value={Qty}
              className="text-center border-[1px] border-slate-200 p-1 px-3 ms-1 me-1 w-[2.5rem]"
              onChange={handleQtyChange}
              onBlur={updateQtyChange}
            />
            <button
              onClick={handleIncreaseQty}
              className="rounded-full border-[1px] border-slate-200 p-1 px-3"
            >
              +
            </button>
          </div>
          <button
            onClick={handleRemoveCartItem}
            className="text-red-600 border-[1px] border-slate-200 p-1 px-3 rounded"
          >
            Remove
          </button>
        </div>
      </div>
    );
  }
  
  export default Item;
  