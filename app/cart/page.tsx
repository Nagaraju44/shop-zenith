/* eslint-disable @next/next/no-img-element */
"use client";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import type { RootState } from "../../store/store";
import Item from "./item";
import CustomModal from "../components/custom_modal";
import { LocateFixed } from "lucide-react";
import { indianStates } from "../utils/contsant";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import Loading from "../components/loading";
import { redirect, useRouter } from "next/navigation";
import { ProductState } from "@/store/productSlice";
import { useAuthContext } from "@/context/AuthContext";

interface Props extends React.ComponentPropsWithoutRef<"div"> {
  open: boolean;
  handleClose: () => void;
}

const schema = yup.object().shape({
  name: yup.string().required("*Name is required"),
  phone: yup
    .string()
    .matches(/^\d+$/, "*Phone number must contain only digits")
    .min(10, "*Phone number must be at least 10 characters")
    .max(10, "*Phone number cannot exceed 10 characters")
    .required("*Phone is required"),
  pincode: yup
    .string()
    .matches(/^\d+$/, "*Pincode must contain only digits")
    .min(6, "*Pincode must be 6 characters")
    .max(6, "*Pincode must be 6 characters")
    .required("*Pincode is required"),
  locality: yup.string().required("*Locality is required"),
  address: yup.string().required("*Address is required"),
  city: yup.string().required("*City/District/Town is required"),
  state: yup.string().required("*State is required"),
});

type Inputs = {
  name: string;
  phone: string;
  pincode: string;
  locality: string;
  address: string;
  city: string;
  state: string;
};

function Cart() {
  const router = useRouter();
  
  const { user }: any = useAuthContext();

  useEffect(() => {
    if (user == null) {
      redirect("/login");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user]);

  const [showAddressAdd, setShowAddressAdd] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [err, setErr] = useState(false);

  const cartItems = useSelector((state: RootState) => state.products.cart);
  const savedAddress = useSelector(
    (state: RootState) => state.products.Addresses
  );

  const {
    register,
    handleSubmit,
    setValue,
    watch,
    formState: { errors },
  } = useForm<Inputs>({
    resolver: yupResolver(schema),
  });

  const Total = cartItems.reduce(
    (acc: number, curr: { price: number; qty: number }) =>
      acc + curr.price * curr.qty,
    0
  );
  const discount = Total / 10;
  const delivery = cartItems.length * 40;

  const checkAddress = () => {
    if (Object.entries(savedAddress).length) {
    } else {
      setShowAddressAdd(true);
    }
  };

  const handleContinue = () => {
    if (cartItems.length === 0) {
      router.push("/");
    } else {
      checkAddress();
    }
  };

  const city = watch("city");
  const state = watch("state");
  const pincode = watch("pincode");

  const getCurrentLocation = () => {
    setLoading(true);
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition);
    } else {
      setLoading(false);
      setErr(true);
    }
  };

  function showPosition(position: any) {
    let lat = position.coords.latitude;
    let lan = position.coords.longitude;
    const url = `https://nominatim.openstreetmap.org/reverse?format=json&lat=${lat}&lon=${lan}`;
    fetch(url)
      .then((res) => res.json())
      .then((data) => {
        setLoading(false);
        setValue("city", data.address.county);
        setValue("state", data?.address?.state?.toLowerCase());
        setValue("pincode", data.address.postcode.toLowerCase());
      })
      .catch((err: any) => {
        setErr(true);
        setLoading(false);
      });
  }

  const onSubmit = (data: any) => {
    console.log("submitted");
  };

  return (
    <div className="min-h-screen flex mt-5 justify-center">
      <div className="w-[60%]">
        <div className="flex justify-between items-center px-3 rounded-md bg-white self-end py-3">
          <p>Enter PinCode</p>
          <button
            onClick={checkAddress}
            className="text-blue-600 border-[1px] border-slate-200 p-1 px-3 rounded"
          >
            Enter Delivery Pincode
          </button>
        </div>
        <div className="bg-white mt-3 rounded-md px-3 py-3">
          {cartItems.length ? (
            cartItems.map((cartItem: ProductState) => {
              return <Item key={cartItem.id} cartItem={cartItem} />;
            })
          ) : (
            <img
              src="https://cdni.iconscout.com/illustration/premium/thumb/empty-cart-5521508-4610092.png?f=webp"
              alt="no-items"
              className="h-[400px] block m-auto"
            />
          )}
          <div className="text-end shadow-2xl pb-4 pe-4 rounded sticky bottom-0 left-0 right-0 z-50 bg-white">
            <button
              onClick={handleContinue}
              className="bg-[#FB641B] hover:bg-orange-500 border-orange-400 hover:border-orange-500 text-white py-4 px-16 border rounded mt-3"
            >
              {cartItems.length !== 0 ? "PLACE ORDER" : "BROWSE ITEMS"}
            </button>
          </div>
        </div>
      </div>
      <div className="w-[25%] ms-3">
        <div className="bg-white p-4 rounded-md shadow-lg">
          <p className="mb-4">PRICE DETAILS</p>
          <hr />
          <div className="flex justify-between mt-4">
            <p>Price ({cartItems.length} items)</p>
            <p>&#8377;{(Total + discount).toFixed(2)}</p>
          </div>
          <div className="flex justify-between mt-4">
            <p>Discount</p>
            <p className="text-green-500">-&#8377;{(Total / 10).toFixed(2)}</p>
          </div>
          <div className="flex justify-between mt-4 mb-4">
            <p>Delivery Charges</p>
            {Total >= 500 ? (
              <p className=" text-gray-500">
                <span className="line-through me-1">&#8377;{delivery}</span>
                <span className="no-underline text-green-500">Free</span>
              </p>
            ) : (
              <p className="">&#8377;{delivery}</p>
            )}
          </div>
          <hr />
          <div className="flex justify-between mt-4 mb-4">
            <p>Total Amount</p>
            <p>{(Total + (Total >= 500 ? 0 : delivery)).toFixed(2)}</p>
          </div>
          <hr />
          <p className="mt-4 text-green-600 font-[500]">
            You will save ₹{discount.toFixed(2)} on this order
          </p>
        </div>
      </div>
      {showAddressAdd && (
        <CustomModal
          title="Add Address"
          open={showAddressAdd}
          onClose={() => setShowAddressAdd(false)}
          content={
            <div className="mt-3">
              <div>
                <button
                  onClick={getCurrentLocation}
                  className="flex items-center bg-blue-500 text-white p-3 rounded"
                >
                  <LocateFixed size={18} className="me-1" />
                  Use My Current Location
                </button>
              </div>
              {isLoading && (
                <div className="absolute top-0 left-0 w-full h-full flex items-center justify-center bg-black bg-opacity-25 z-20">
                  <Loading />
                </div>
              )}
              <div className="flex flex-wrap justify-between mt-4 relative">
                <div className="w-[49%]">
                  <input
                    type="text"
                    id="name"
                    placeholder="Name"
                    {...register("name")}
                    className="w-[100%] p-3 border-[1px] border-solid border-blue-500 rounded focus:outline-none text-[#000000] opacity-100"
                  />
                  {errors?.name?.message ? (
                    <p className="mb-1 text-red-500 text-[11px]">
                      {errors?.name?.message}
                    </p>
                  ) : (
                    <p className="mb-1 text-red-500 text-[11px]">&nbsp;</p>
                  )}
                </div>
                <div className="w-[49%]">
                  <input
                    type="text"
                    id="phone"
                    {...register("phone")}
                    placeholder="Phone"
                    className="w-[100%] p-3 border-[1px] border-solid border-blue-500 rounded focus:outline-none text-[#000000] opacity-100"
                  />
                  {errors?.phone?.message ? (
                    <p className="mb-1 text-red-500 text-[11px]">
                      {errors?.phone?.message}
                    </p>
                  ) : (
                    <p className="mb-1 text-red-500 text-[11px]">&nbsp;</p>
                  )}
                </div>
                <div className="w-[49%]">
                  <input
                    type="text"
                    value={pincode}
                    id="pincode"
                    {...register("pincode")}
                    placeholder="Pincode"
                    className="w-[100%] p-3 border-[1px] border-solid border-blue-500 rounded focus:outline-none text-[#000000] opacity-100"
                  />
                  {errors?.pincode?.message ? (
                    <p className="mb-1 text-red-500 text-[11px]">
                      {errors?.pincode?.message}
                    </p>
                  ) : (
                    <p className="mb-1 text-red-500 text-[11px]">&nbsp;</p>
                  )}
                </div>
                <div className="w-[49%]">
                  <input
                    type="text"
                    id="locality"
                    {...register("locality")}
                    placeholder="Locality"
                    className="w-[100%] p-3 border-[1px] border-solid border-blue-500 rounded focus:outline-none text-[#000000] opacity-100"
                  />
                  {errors?.locality?.message ? (
                    <p className="mb-1 text-red-500 text-[11px]">
                      {errors?.locality?.message}
                    </p>
                  ) : (
                    <p className="mb-1 text-red-500 text-[11px]">&nbsp;</p>
                  )}
                </div>
                <div className="w-[49%]">
                  <textarea
                    id="address"
                    {...register("address")}
                    placeholder="Address"
                    className="w-[100%] resize-none p-3 border-[1px] border-solid border-blue-500 rounded focus:outline-none text-[#000000] opacity-100"
                  ></textarea>
                  {errors?.address?.message ? (
                    <p className="mb-1 text-red-500 text-[11px]">
                      {errors?.address?.message}
                    </p>
                  ) : (
                    <p className="mb-1 text-red-500 text-[11px]">&nbsp;</p>
                  )}
                </div>
                <div className="w-[49%]">
                  <input
                    type="text"
                    value={city}
                    id="city"
                    {...register("city")}
                    placeholder="City/District/Town"
                    className="w-[100%] h-[2.75rem] p-3 border-[1px] border-solid border-blue-500 rounded focus:outline-none text-[#000000] opacity-100"
                  />
                  {errors?.city?.message ? (
                    <p className="mb-1 text-red-500 text-[11px]">
                      {errors?.city?.message}
                    </p>
                  ) : (
                    <p className="mb-1 text-red-500 text-[11px]">&nbsp;</p>
                  )}
                </div>
                <div className="w-[49%]">
                  <select
                    {...register("state")}
                    value={state}
                    id="state"
                    className="w-[100%] p-3 border-[1px] border-solid border-blue-500 rounded focus:outline-none text-[#000000] opacity-100"
                  >
                    {indianStates.map((state) => (
                      <option key={state.value} value={state.value}>
                        {state.name}
                      </option>
                    ))}
                  </select>
                  {errors?.state?.message ? (
                    <p className="mb-1 text-red-500 text-[11px]">
                      {errors?.state?.message}
                    </p>
                  ) : (
                    <p className="mb-1 text-red-500 text-[11px]">&nbsp;</p>
                  )}
                </div>
              </div>
              <div className="flex justify-end">
                <button
                  className="bg-red-700 text-white p-3 rounded me-2"
                  onClick={() => setShowAddressAdd(false)}
                >
                  CANCEL
                </button>
                <button
                  onClick={handleSubmit(onSubmit)}
                  className="bg-[#FB641B] hover:bg-orange-500 border-orange-400 hover:border-orange-500 text-white p-3 border rounded"
                >
                  SAVE AND DELIVER HERE
                </button>
              </div>
            </div>
          }
        />
      )}
      {err && (
        <CustomModal
          title="Error"
          size="max-w-md"
          open={err}
          onClose={() => setErr(false)}
          content={
            <div className="mt-3">
              <img
                src="https://cdn.pixabay.com/photo/2017/02/12/21/29/false-2061132_640.png"
                alt="fail"
                className="object-fill mix-blend-multiply image-filter"
              />
              <h3 className="mb-1 text-xl text-center text-black font-bold shadow-none">
                Unable to Get Information. Please Add Address Manually
              </h3>
              <div className="flex justify-center mt-3">
                <button
                  className="bg-gray-300 text-black p-3 rounded me-2 font-bold"
                  onClick={() => setErr(false)}
                >
                  Close
                </button>
              </div>
            </div>
          }
        />
      )}
    </div>
  );
}

export default Cart;
