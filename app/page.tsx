"use client";
import InfoCard from "./components/info";
import ShowCase from "./components/showcase";
import Footer from "./components/footer";
import Loading from "./components/loading";
import { Suspense, useEffect } from "react";
import Product from "./components/product";
import { useAuthContext } from "@/context/AuthContext";
import { redirect } from 'next/navigation'
import { gsap } from "gsap";
import { useGSAP } from "@gsap/react";
    
import { TextPlugin } from "gsap/TextPlugin";


gsap.registerPlugin(useGSAP,TextPlugin);

export default function Home() {
  const { user }: any = useAuthContext();
  
  useEffect(() => {
    if (user == null) {
      redirect('/login')
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user]);

  return (
    <main>
      <InfoCard />
      <ShowCase />
      <Suspense fallback={<Loading />}>
        <Product />
      </Suspense>
      <Footer />
    </main>
  );
}
