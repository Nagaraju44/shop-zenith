"use client";
import React, { useEffect } from "react";
import { useAuthContext } from "@/context/AuthContext";
import { redirect } from "next/navigation";

function Sales() {
  const { user }: any = useAuthContext();

  useEffect(() => {
    if (user == null) {
      redirect("/login");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user]);

  return (
    <div className="min-h-screen">
      <div className="flex justify-center items-center h-[80vh]">
        <h1>Coming Soon...</h1>
      </div>
    </div>
  );
}

export default Sales;
