export async function getData() {
  const res = await fetch("https://fakestoreapi.com/products", {
    cache: "force-cache",
    next: { revalidate: 3600 },
  });
  if (!res.ok) {
    throw new Error("Failed to fetch data");
  }
  return res.json();
}

// pages/api/video.js

export function handler(req: any, res: any) {
  res.setHeader("Cache-Control", "public, max-age=3600");
  const videoPath = "pexels.mp4";
  res.sendFile(videoPath);
}
