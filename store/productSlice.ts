import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import { act } from "react-dom/test-utils";

export interface ProductState {
  id: number;
  title: string;
  price: number;
  description: string;
  image: string;
  category: string;
  rating: any;
  isCartAdded: boolean;
  qty: number;
}

interface MyArrayInterface {
  products: ProductState[];
  cart: ProductState[];
  Addresses: any;
}

const initialState: MyArrayInterface = {
  products: [],
  cart: [],
  Addresses: {},
};

export const productSlice = createSlice({
  name: "products",
  initialState,
  reducers: {
    addProducts: (state, action: PayloadAction<ProductState[]>) => {
      let updatedProducts = action.payload.map((product) => {
        return { ...product, isCartAdded: false, qty: 1 };
      });
      state.products = updatedProducts;
    },
    addToCart: (state, action: PayloadAction<ProductState>) => {
      state.cart = [...state.cart, action.payload];
      let updatedProducts = state.products.map((product) => {
        if (product.id === action.payload.id) {
          return {
            ...product,
            isCartAdded: true,
          };
        }
        return product;
      });
      state.products = updatedProducts;
    },
    removeFromCart: (state, action: PayloadAction<number>) => {
      state.cart = state.cart.filter(
        (product) => product.id !== action.payload
      );
      let updatedProducts = state.products.map((product) => {
        if (product.id === action.payload) {
          return {
            ...product,
            isCartAdded: false,
            qty: 1,
          };
        }
        return product;
      });
      state.products = updatedProducts;
    },
    IncreaseQty: (state, action) => {
      let updatedProducts = state.products.map((product) => {
        if (product.id === action.payload) {
          return {
            ...product,
            qty: product.qty + 1,
          };
        }
        return product;
      });
      let updatedCart = state.cart.map((product) => {
        if (product.id === action.payload) {
          return {
            ...product,
            qty: product.qty + 1,
          };
        }
        return product;
      });
      state.products = updatedProducts;
      state.cart = updatedCart;
    },
    decreaseQty: (state, action) => {
      let updatedProducts = state.products.map((product) => {
        if (product.id === action.payload && product.qty > 1) {
          return {
            ...product,
            qty: product.qty - 1,
          };
        }
        return product;
      });
      let updatedCart = state.cart.map((product) => {
        if (product.id === action.payload && product.qty > 1) {
          return {
            ...product,
            qty: product.qty - 1,
          };
        }
        return product;
      });
      state.products = updatedProducts;
      state.cart = updatedCart;
    },
    EnterQty: (state, action) => {
      let updatedProducts = state.products.map((product) => {
        if (product.id === action.payload.id) {
          return {
          ...product,
            qty: action.payload.qty,
          };
        }
        return product;
      });
      let updatedCart = state.cart.map((product) => {
        if (product.id === action.payload.id) {
          return {
          ...product,
            qty: action.payload.qty,
          };
        }
        return product;
      });
      state.products = updatedProducts;
      state.cart = updatedCart;
    },
    addAddress: (state, action) => {
      state.Addresses = action.payload;
    }
  },
});

export const {
  addProducts,
  addToCart,
  removeFromCart,
  IncreaseQty,
  decreaseQty,
  EnterQty,
  addAddress
} = productSlice.actions;

export default productSlice.reducer;
